version: '2'

services:
  # a sleep container to allow an early docker-compose logs -f
  pause:
    image: bash:5.1.4
    command: sleep infinity

  # pre-populate volumes with dump.rdb
  copy-dump-rdb:
    image: alpine:3.13
    command:
      - /bin/sh
      - -c
      - >
        mkdir -p /mnt/redis_data-01/redis/data /mnt/redis_data-02/redis/data /mnt/redis_data-03/redis/data /mnt/redis_integrity-01/redis/data;
        chown 1001 /mnt/redis_data-01/redis/data /mnt/redis_data-02/redis/data /mnt/redis_data-03/redis/data /mnt/redis_integrity-01/redis/data;
        cp /mnt/data/dump.rdb /mnt/redis_data-01/redis/data;
        cp /mnt/data/dump.rdb /mnt/redis_data-02/redis/data;
        cp /mnt/data/dump.rdb /mnt/redis_data-03/redis/data;
        cp /mnt/data/dump.rdb /mnt/redis_integrity-01/redis/data;
    volumes:
      - ./data:/mnt/data:ro
      - redis_data-01:/mnt/redis_data-01
      - redis_data-02:/mnt/redis_data-02
      - redis_data-03:/mnt/redis_data-03
      - redis_integrity-01:/mnt/redis_integrity-01

  save-dump-rdb:
    image: alpine:3.13
    entrypoint:
      - /bin/sh
      - -c
      - >
        mkdir -p /mnt/redis_integrity-02/redis/data;
        chown 1001 /mnt/redis_integrity-02/redis/data;
        cp "/mnt/redis_data-$${0}/redis/data/dump.rdb" /mnt/redis_integrity-02/redis/data/dump.rdb;
    volumes:
      - redis_data-01:/mnt/redis_data-01
      - redis_data-02:/mnt/redis_data-02
      - redis_data-03:/mnt/redis_data-03
      - redis_integrity-02:/mnt/redis_integrity-02

  downgrade-fix-config-sentinel:
    image: alpine:3.13
    entrypoint:
      - /bin/sh
      - -c
      - >
        sed -i '/^user /d' "/mnt/sentinel_data-$${0}/redis-sentinel/conf/sentinel.conf"
    volumes:
      - redis-sentinel_data-01:/mnt/sentinel_data-01
      - redis-sentinel_data-02:/mnt/sentinel_data-02
      - redis-sentinel_data-03:/mnt/sentinel_data-03

  downgrade-fix-config-redis:
    image: alpine:3.13
    entrypoint:
      - /bin/sh
      - -c
      - >
        sed -i '/^user /d' "/mnt/redis_etc-$${0}/redis.conf"
    volumes:
      - redis_etc-01:/mnt/redis_etc-01
      - redis_etc-02:/mnt/redis_etc-02
      - redis_etc-03:/mnt/redis_etc-03

  redis-integrity-01:
    image: docker.io/bitnami/redis:6.0.16
    volumes:
      - redis_integrity-01:/bitnami
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
    networks:
      vpcbr:
        ipv4_address: 172.20.3.101

  redis-integrity-02:
    image: docker.io/bitnami/redis:6.2.6
    volumes:
      - redis_integrity-02:/bitnami
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
    networks:
      vpcbr:
        ipv4_address: 172.20.3.102

  ruby-client-integrity:
    image: igorwgitlab/redis-upgrade-harness-ruby-client
    entrypoint: "ruby integrity.rb"
    environment:
      - REDIS_01_URL=redis://172.20.3.101
      - REDIS_02_URL=redis://172.20.3.102
    networks:
      vpcbr:
        ipv4_address: 172.20.3.111

  # redis instance for generating test data
  redis-generate-01:
    image: docker.io/bitnami/redis:6.0.16
    volumes:
      - redis_generate-01:/bitnami
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
    networks:
      vpcbr:
        ipv4_address: 172.20.3.103

  save-dump-rdb-generate-01:
    image: alpine:3.13
    entrypoint:
      - /bin/sh
      - -c
      - -x
      - >
        ls /mnt/data/dump.rdb;
        ls -l /mnt/data/dump.rdb;
        ls -lah /mnt/data/dump.rdb;
        [ -f /mnt/data/dump.rdb ] && echo 'error: data/dump.rdb already exists, not overwriting' && exit 1;
        cp /mnt/redis_generate-01/redis/data/dump.rdb /mnt/data/dump.rdb;
    volumes:
      - ./data:/mnt/data
      - redis_generate-01:/mnt/redis_generate-01

  # redis sentinel 5
  redis-sentinel-60-01:
    image: docker.io/bitnami/redis-sentinel:6.0.16
    environment:
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis-sentinel_data-01:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.11
  redis-sentinel-60-02:
    image: docker.io/bitnami/redis-sentinel:6.0.16
    environment:
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis-sentinel_data-02:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.12
  redis-sentinel-60-03:
    image: docker.io/bitnami/redis-sentinel:6.0.16
    environment:
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis-sentinel_data-03:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.13

  # redis sentinel 6
  redis-sentinel-62-01:
    image: docker.io/bitnami/redis-sentinel:6.2.6
    environment:
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis-sentinel_data-01:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.11
  redis-sentinel-62-02:
    image: docker.io/bitnami/redis-sentinel:6.2.6
    environment:
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis-sentinel_data-02:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.12
  redis-sentinel-62-03:
    image: docker.io/bitnami/redis-sentinel:6.2.6
    environment:
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis-sentinel_data-03:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.13

  # redis 6.0
  redis-60-01:
    image: docker.io/bitnami/redis:6.0.16
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
      - REDIS_REPLICATION_MODE=master
    volumes:
      - redis_etc-01:/opt/bitnami/redis/etc
      - redis_data-01:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.101
  redis-60-02:
    image: docker.io/bitnami/redis:6.0.16
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
      - REDIS_REPLICATION_MODE=slave
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis_etc-02:/opt/bitnami/redis/etc
      - redis_data-02:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.102
  redis-60-03:
    image: docker.io/bitnami/redis:6.0.16
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
      - REDIS_REPLICATION_MODE=slave
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis_etc-03:/opt/bitnami/redis/etc
      - redis_data-03:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.103

  # redis 6.2
  redis-62-01:
    image: docker.io/bitnami/redis:6.2.6
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
      - REDIS_REPLICATION_MODE=master
    volumes:
      - redis_etc-01:/opt/bitnami/redis/etc
      - redis_data-01:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.101
  redis-62-02:
    image: docker.io/bitnami/redis:6.2.6
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
      - REDIS_REPLICATION_MODE=slave
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis_etc-02:/opt/bitnami/redis/etc
      - redis_data-02:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.102
  redis-62-03:
    image: docker.io/bitnami/redis:6.2.6
    environment:
      - REDIS_AOF_ENABLED=no
      - ALLOW_EMPTY_PASSWORD=yes
      - REDIS_REPLICATION_MODE=slave
      - REDIS_MASTER_HOST=172.20.1.101
    volumes:
      - redis_etc-03:/opt/bitnami/redis/etc
      - redis_data-03:/bitnami
    networks:
      vpcbr:
        ipv4_address: 172.20.1.103

  # client workload
  ruby-client:
    image: igorwgitlab/redis-upgrade-harness-ruby-client
    environment:
      - REDIS_URL=redis://mymaster
      - REDIS_SENTINELS=172.20.1.11:26379,172.20.1.12:26379,172.20.1.13:26379
    networks:
      vpcbr:
        ipv4_address: 172.20.1.2
  ruby-client-write-sequence:
    image: igorwgitlab/redis-upgrade-harness-ruby-client
    command: write-sequence
    environment:
      - REDIS_URL=redis://mymaster
      - REDIS_SENTINELS=172.20.1.11:26379,172.20.1.12:26379,172.20.1.13:26379
    networks:
      vpcbr:
        ipv4_address: 172.20.1.3
  ruby-client-read-sequence:
    image: igorwgitlab/redis-upgrade-harness-ruby-client
    command: read-sequence
    environment:
      - REDIS_URL=redis://mymaster
      - REDIS_SENTINELS=172.20.1.11:26379,172.20.1.12:26379,172.20.1.13:26379
    networks:
      vpcbr:
        ipv4_address: 172.20.1.4

volumes:
  redis-sentinel_data-01:
    driver: local
  redis-sentinel_data-02:
    driver: local
  redis-sentinel_data-03:
    driver: local
  redis_etc-01:
    driver: local
  redis_etc-02:
    driver: local
  redis_etc-03:
    driver: local
  redis_data-01:
    driver: local
  redis_data-02:
    driver: local
  redis_data-03:
    driver: local
  redis_integrity-01:
    driver: local
  redis_integrity-02:
    driver: local
  redis_generate-01:
    driver: local

networks:
  vpcbr:
    driver: bridge
    ipam:
     config:
       - subnet: 172.20.0.0/16
         gateway: 172.20.1.1
