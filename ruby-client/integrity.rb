require 'redis'
require 'set'

$stdout.sync = true
$stderr.sync = true

redis_01 = Redis.new(
  url: ENV['REDIS_01_URL'],
)

redis_02 = Redis.new(
  url: ENV['REDIS_02_URL'],
)

puts "Preparing dataset"

found = 0
deleted = 0
redis_01.scan_each(count: 1000).each_slice(1000) do |keys|
  ttls = []
  redis_01.pipelined do
    ttls = keys.zip(keys.map { |key| redis_01.ttl(key) })
  end

  to_del = ttls.map { |k, v| [k, v.value] }.select { |k, v| v != -1 }.to_h.keys
  found += to_del.size
  deleted += redis_01.del(*to_del) if to_del.size > 0
end
puts "msg=\"del-keys-with-ttl complete\" found=#{found} deleted=#{deleted}"

redis_02.del('redis-upgrade-harness:sequence')

puts "Starting data integrity check..."

check_pct = 10
dbsize = redis_01.dbsize
key_check_count = (dbsize * (check_pct / 100.0)).to_i

puts "dbsize_01=#{dbsize} dbsize_02=#{redis_02.dbsize} check_pct=#{check_pct} key_check_count=#{key_check_count}"

key_check_count.times do
  key = redis_01.randomkey

  if !redis_02.exists?(key)
    raise "Key doesn't exist in redis_integrity-02. Key: #{key}"
  end

  type = redis_01.type(key)
  case type
  when "string"
    val1 = redis_01.get(key)
    val2 = redis_02.get(key)
  when "list"
    val1 = redis_01.lrange(key, 0, -1)
    val2 = redis_02.lrange(key, 0, -1)
  when "set"
    val1 = redis_01.smembers(key).to_set
    val2 = redis_02.smembers(key).to_set
  when "zset"
    val1 = redis_01.zrange(key, 0, -1, with_scores: true)
    val2 = redis_02.zrange(key, 0, -1, with_scores: true)
  when "hash"
    val1 = redis_01.hgetall(key).sort.to_h
    val2 = redis_02.hgetall(key).sort.to_h
  else
    raise "Unknown key type. The key type received is: #{type}. Key: #{key}"
  end

  if val1 != val2
    puts "Value mismatch."
    puts key
    puts val1.inspect
    puts val2.inspect
    exit 1
  end
end

puts "...data integrity check successful."
