require 'redis'
require 'uri'

$stdout.sync = true
$stderr.sync = true

module ConnectorExtensions
  def resolve
    begin
      val = super
    ensure
      puts "connector resolved to: #{val}"
    end
    val
  end
end

Redis::Client::Connector::Sentinel.prepend(ConnectorExtensions)

sentinel = Redis.new(url: 'redis://' + ENV['REDIS_SENTINELS'].split(',').first)

redis = Redis.new(
  sentinels: ENV['REDIS_SENTINELS'].split(',').map { |hostport|
    u = URI.parse('redis://' + hostport)
    { host: u.host, port: u.port, username: u.user, password: u.password }
  },
)

mode = ARGV.shift
case mode
when "del-keys-with-ttl"
  # delete all keys with ttl so that we have a consistent set of keys
  found = 0
  deleted = 0
  redis.scan_each(count: 1000).each_slice(1000) do |keys|
    ttls = []
    redis.pipelined do
      ttls = keys.zip(keys.map { |key| redis.ttl(key) })
    end

    to_del = ttls.map { |k, v| [k, v.value] }.select { |k, v| v != -1 }.to_h.keys
    found += to_del.size
    deleted += redis.del(*to_del) if to_del.size > 0
  end
  puts "msg=\"del-keys-with-ttl complete\" found=#{found} deleted=#{deleted}"
when "write-sequence"
  i = 0
  loop do
    redis.rpush('redis-upgrade-harness:sequence', i)
    i += 1
    # if i % 1000 == 0
    #   puts "writing #{i}"
    # end
  end
when "read-sequence"
  writes_ok = redis.llen('redis-upgrade-harness:sequence')
  writes_attempted = redis.lindex('redis-upgrade-harness:sequence', -1).to_i + 1
  writes_lost_ratio = 1.0 - (writes_ok.to_f / writes_attempted.to_f)
  puts "msg=\"read-sequence complete\" writes_ok=#{writes_ok} writes_attempted=#{writes_attempted} writes_lost_percent=#{(writes_lost_ratio*100).to_i}"
when "sentinel-link-status"
  l = sentinel.call(['sentinel', 'replicas', 'mymaster']).map { |x| x.each_slice(2).to_h }

  ip = ARGV.shift
  if ip
    if x = l.find { |x| x["ip"] == ip }
      puts x["master-link-status"]
      return
    else
      puts "error: ip #{ip} not found in sentinel replicas"
      exit 1
    end
  end

  puts l.map { |x| [x["ip"], x["master-link-status"]] }.flatten.join("\n")
when "sidekiq-queues-scard"
  puts redis.scard('resque:gitlab:queues').inspect
else
  raise "unknown mode #{mode.inspect} provided"
end
