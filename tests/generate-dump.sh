#!/usr/bin/env bash

# script to generate a dump.rdb file

set -e

ls -lah data

docker-compose down --volumes

docker-compose up --no-start redis-generate-01

ls -lah data

docker-compose start redis-generate-01
docker-compose exec redis-generate-01 redis-cli debug populate 10000
docker-compose exec redis-generate-01 redis-cli save
docker-compose stop redis-generate-01

ls -lah data

docker-compose run save-dump-rdb-generate-01

ls -lah data

docker-compose down
