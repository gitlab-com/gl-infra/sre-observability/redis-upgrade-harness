# library of gnarly logic

log_grey () {
    echo $(tput bold)$(tput setaf 0)${@}$(tput sgr 0)
}
log_norm () {
    echo $(tput bold)$(tput setaf 9)${@}$(tput sgr 0)
}
log_info () {
    echo $(tput bold)$(tput setaf 4)${@}$(tput sgr 0)
}
log_warn () {
    echo $(tput bold)$(tput setaf 2)${@}$(tput sgr 0)
}
log_err ()  {
    echo $(tput bold)$(tput setaf 1)${@}$(tput sgr 0)
}

function redis_upgrade {
  log_grey "redis_upgrade $@"
  local i="$1"
  local service_sentinel="${2:-redis-sentinel-62-01}"
  if docker-compose exec redis-60-$i redis-cli role | head -n 1 | grep master >/dev/null; then
    docker-compose exec $service_sentinel redis-cli -p 26379 sentinel failover mymaster
    redis_wait_for_stepdown_or_exit redis-60-$i
    redis_wait_for_connected_or_exit redis-60-$i
  fi
  docker-compose stop redis-60-$i
  docker-compose start redis-62-$i
  redis_wait_for_pong_or_exit redis-62-$i
  sleep 3
}

function redis_downgrade {
  log_grey "redis_downgrade $@"
  local i="$1"
  local service_sentinel="${2:-redis-sentinel-62-01}"
  if docker-compose exec redis-62-$i redis-cli role | head -n 1 | grep master >/dev/null; then
    docker-compose exec $service_sentinel redis-cli -p 26379 sentinel failover mymaster
    redis_wait_for_stepdown_or_exit redis-62-$i
    redis_wait_for_connected_or_exit redis-62-$i
  fi
  docker-compose run downgrade-fix-config-redis $i
  docker-compose stop redis-62-$i
  docker-compose start redis-60-$i
  redis_wait_for_pong_or_exit redis-60-$i
  sleep 3
}

function redis_wait_for_pong {
  log_grey "redis_wait_for_pong $@"
  local service="$1"
  local i
  for i in {1..30}
  do
    OUTPUT="$(docker-compose exec "$service" redis-cli ping)"
    if [[ "$OUTPUT" == *PONG* ]]
    then
      return 0
    fi
    sleep 1
  done
  return 1
}

function redis_wait_for_stepdown {
  log_grey "redis_wait_for_stepdown $@"
  local service="$1"
  local i
  for i in {1..60}
  do
    OUTPUT="$(docker-compose exec "$service" redis-cli role)"
    if [[ "$OUTPUT" == *slave* ]]
    then
      return 0
    fi
    sleep 1
  done
  return 1
}

function redis_wait_for_connected {
  log_grey "redis_wait_for_connected $@"
  local service="$1"
  local i
  local lines
  for i in {1..60}
  do
    OUTPUT="$(docker-compose exec "$service" redis-cli --raw role | tr -d '\r' )"
    readarray -t lines <<<"$OUTPUT"
    if [[ "${lines[0]}" == *slave* ]] && [[ "${lines[3]}" == *connected* ]]
    then
      return 0
    elif [[ "${lines[0]}" == *master* ]]
    then
      return 0
    fi
    sleep 1
  done
  return 1
}

function sentinel_wait_for_pong {
  log_grey "sentinel_wait_for_pong $@"
  local service="$1"
  local i
  for i in {1..30}
  do
    OUTPUT="$(docker-compose exec "$service" redis-cli -p 26379 ping)"
    if [[ "$OUTPUT" == *PONG* ]]
    then
      return 0
    fi
    sleep 1
  done
  return 1
}

function sentinel_wait_for_master {
  log_grey "sentinel_wait_for_master $@"
  local service="$1"
  local ip="$2"
  local i
  for i in {1..60}
  do
    OUTPUT="$(docker-compose exec "$service" redis-cli --raw -p 26379 sentinel get-master-addr-by-name mymaster | tr -d '\r' | head -n 1)"
    if [[ "$OUTPUT" == "$ip" ]]
    then
      return 0
    fi
    sleep 1
  done
  return 1
}

function sentinel_wait_for_replica {
  log_grey "sentinel_wait_for_replica $@"
  local ip="$1"
  local i
  for i in {1..60}
  do
    OUTPUT="$(docker-compose run ruby-client sentinel-link-status "$ip" | tr -d '\r')"
    if [[ $? == 0 ]] && [[ "$OUTPUT" == 'ok' ]]
    then
      return 0
    fi
    sleep 1
  done
  return 1
}

function redis_wait_for_pong_or_exit {
  local service="$1"
  redis_wait_for_pong "$service" || (echo "timed out waiting for $service pong" && exit 1)
}

function redis_wait_for_stepdown_or_exit {
  local service="$1"
  redis_wait_for_stepdown "$service" || (echo "timed out waiting for $service stepdown" && exit 1)
}

function redis_wait_for_connected_or_exit {
  local service="$1"
  redis_wait_for_connected "$service" || (echo "timed out waiting for $service to connect to master" && exit 1)
}

function sentinel_wait_for_pong_or_exit {
  local service="$1"
  sentinel_wait_for_pong "$service" || (echo "timed out waiting for $service pong" && exit 1)
}

function sentinel_wait_for_master_or_exit {
  local service="$1"
  local ip="$2"
  sentinel_wait_for_master "$service" "$ip" || (echo "timed out waiting for $ip to report as master in sentinel" && exit 1)
}

function sentinel_wait_for_replica_or_exit {
  local ip="$1"
  sentinel_wait_for_replica "$ip" || (echo "timed out waiting for $ip to report as replica in sentinel" && exit 1)
}
