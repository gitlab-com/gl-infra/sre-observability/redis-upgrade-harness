#!/usr/bin/env bash

# upgrade sentinel and redis in lockstep
# this means both redis and sentinel will be running mixed-mode
# then downgrade all

echo "test: $0"

set -e

# shellcheck source=tests/lib.sh
. "$(dirname "$0")/lib.sh"

docker-compose down --volumes

docker-compose run copy-dump-rdb

docker-compose up --no-start pause
docker-compose up --no-start redis-60-01 redis-60-02 redis-60-03
docker-compose up --no-start redis-62-01 redis-62-02 redis-62-03
docker-compose up --no-start redis-sentinel-60-01 redis-sentinel-60-02 redis-sentinel-60-03
docker-compose up --no-start redis-sentinel-62-01 redis-sentinel-62-02 redis-sentinel-62-03
docker-compose up --no-start ruby-client-write-sequence
docker-compose up --no-start redis-integrity-01 redis-integrity-02

# TODO: consider dumping these in a file
# docker-compose logs -f &

log_info "starting"
docker-compose start pause
sleep 5

docker-compose start redis-60-01 redis-60-02 redis-60-03
docker-compose start redis-sentinel-60-01 redis-sentinel-60-02 redis-sentinel-60-03

redis_wait_for_connected_or_exit redis-60-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-60-03
sentinel_wait_for_pong_or_exit redis-sentinel-60-01
sentinel_wait_for_pong_or_exit redis-sentinel-60-02
sentinel_wait_for_pong_or_exit redis-sentinel-60-03
docker-compose exec redis-sentinel-60-01 redis-cli -p 26379 sentinel ckquorum mymaster
log_info "Startup completed."

log_info del-keys-with-ttl
docker-compose run ruby-client del-keys-with-ttl

log_info write-sequence
docker-compose start ruby-client-write-sequence

log_info dbsize
docker-compose exec redis-60-01 redis-cli dbsize

log_info upgrading redis-sentinel-01
docker-compose exec redis-sentinel-60-01 redis-cli -p 26379 sentinel flushconfig
docker-compose stop redis-sentinel-60-01
docker-compose start redis-sentinel-62-01
sentinel_wait_for_pong_or_exit redis-sentinel-62-01
docker-compose exec redis-sentinel-62-01 redis-cli -p 26379 sentinel ckquorum mymaster

log_info upgrading redis-01
redis_upgrade 01
redis_wait_for_connected_or_exit redis-62-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-60-03

log_info upgrading redis-sentinel-02
docker-compose exec redis-sentinel-60-02 redis-cli -p 26379 sentinel flushconfig
docker-compose stop redis-sentinel-60-02
docker-compose start redis-sentinel-62-02
sentinel_wait_for_pong_or_exit redis-sentinel-62-02
docker-compose exec redis-sentinel-62-02 redis-cli -p 26379 sentinel ckquorum mymaster

log_info upgrading redis-02
redis_upgrade 02
redis_wait_for_connected_or_exit redis-62-01
redis_wait_for_connected_or_exit redis-62-02
redis_wait_for_connected_or_exit redis-60-03

log_info upgrading redis-sentinel-03
docker-compose exec redis-sentinel-60-03 redis-cli -p 26379 sentinel flushconfig
docker-compose stop redis-sentinel-60-03
docker-compose start redis-sentinel-62-03
sentinel_wait_for_pong_or_exit redis-sentinel-62-03
docker-compose exec redis-sentinel-62-03 redis-cli -p 26379 sentinel ckquorum mymaster

log_info upgrading redis-03
redis_upgrade 03
redis_wait_for_connected_or_exit redis-62-01
redis_wait_for_connected_or_exit redis-62-02
redis_wait_for_connected_or_exit redis-62-03

docker-compose run ruby-client sidekiq-queues-scard

log_info downgrading redis-sentinel-01
docker-compose exec redis-sentinel-62-01 redis-cli -p 26379 sentinel flushconfig
docker-compose stop redis-sentinel-62-01
docker-compose run downgrade-fix-config-sentinel 01
docker-compose start redis-sentinel-60-01
sentinel_wait_for_pong_or_exit redis-sentinel-60-01
docker-compose exec redis-sentinel-60-01 redis-cli -p 26379 sentinel ckquorum mymaster

log_info downgrading redis-01
redis_downgrade 01 redis-sentinel-60-01
redis_wait_for_connected_or_exit redis-60-01
redis_wait_for_connected_or_exit redis-62-02
redis_wait_for_connected_or_exit redis-62-03

log_info downgrading redis-sentinel-02
docker-compose exec redis-sentinel-62-02 redis-cli -p 26379 sentinel flushconfig
docker-compose stop redis-sentinel-62-02
docker-compose run downgrade-fix-config-sentinel 02
docker-compose start redis-sentinel-60-02
sentinel_wait_for_pong_or_exit redis-sentinel-60-02
docker-compose exec redis-sentinel-60-02 redis-cli -p 26379 sentinel ckquorum mymaster

log_info downgrading redis-02
redis_downgrade 02 redis-sentinel-60-01
redis_wait_for_connected_or_exit redis-60-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-62-03

log_info downgrading redis-sentinel-03
docker-compose exec redis-sentinel-62-03 redis-cli -p 26379 sentinel flushconfig
docker-compose stop redis-sentinel-62-03
docker-compose run downgrade-fix-config-sentinel 03
docker-compose start redis-sentinel-60-03
sentinel_wait_for_pong_or_exit redis-sentinel-60-03
docker-compose exec redis-sentinel-60-03 redis-cli -p 26379 sentinel ckquorum mymaster

log_info downgrading redis-03
redis_downgrade 03 redis-sentinel-60-01
redis_wait_for_connected_or_exit redis-60-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-60-03

docker-compose run ruby-client sidekiq-queues-scard

log_info read-sequence
docker-compose stop ruby-client-write-sequence
docker-compose run ruby-client-read-sequence

# data integrity check
log_info dbsize
docker-compose exec redis-60-01 redis-cli dbsize
docker-compose exec redis-60-01 redis-cli save
docker-compose run save-dump-rdb "01"
docker-compose start redis-integrity-01
docker-compose start redis-integrity-02
redis_wait_for_pong_or_exit redis-integrity-01
redis_wait_for_pong_or_exit redis-integrity-02
docker-compose run ruby-client-integrity

docker-compose down

# wait for docker-compose logs -f to complete
# wait

log_info 'done'
