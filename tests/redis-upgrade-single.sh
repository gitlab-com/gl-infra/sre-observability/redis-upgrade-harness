#!/usr/bin/env bash

# start with sentinels already running 6
# upgrade a single node to redis 6
# then test failovers to this node and back to others

echo "test: $0"

set -e

# shellcheck source=tests/lib.sh
. "$(dirname "$0")/lib.sh"

docker-compose down --volumes

docker-compose run copy-dump-rdb

# We create containers ahead of time, `docker-compose start` expects these containers
# to exist. This allows us to maintain symmetry by using `docker-compose start` and
# `docker-compose stop`, without having to worry about whether containers exist yet.
docker-compose up --no-start pause
docker-compose up --no-start redis-60-01 redis-60-02 redis-60-03
docker-compose up --no-start redis-62-01 redis-62-02 redis-62-03
docker-compose up --no-start redis-sentinel-60-01 redis-sentinel-60-02 redis-sentinel-60-03
docker-compose up --no-start redis-sentinel-62-01 redis-sentinel-62-02 redis-sentinel-62-03
docker-compose up --no-start ruby-client-write-sequence
docker-compose up --no-start redis-integrity-01 redis-integrity-02

# TODO: consider dumping these in a file
# docker-compose logs -f &

log_info "starting"
docker-compose start pause
sleep 5

docker-compose start redis-60-01 redis-60-02 redis-60-03
docker-compose start redis-sentinel-62-01 redis-sentinel-62-02 redis-sentinel-62-03

redis_wait_for_connected_or_exit redis-60-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-60-03
sentinel_wait_for_pong_or_exit redis-sentinel-62-01
sentinel_wait_for_pong_or_exit redis-sentinel-62-02
sentinel_wait_for_pong_or_exit redis-sentinel-62-03
docker-compose exec redis-sentinel-62-01 redis-cli -p 26379 sentinel ckquorum mymaster
log_info "Startup completed."

log_info del-keys-with-ttl
docker-compose run ruby-client del-keys-with-ttl

log_info write-sequence
docker-compose start ruby-client-write-sequence

log_info dbsize
docker-compose exec redis-60-01 redis-cli dbsize

log_info upgrading redis-01
redis_upgrade 01
redis_wait_for_connected_or_exit redis-62-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-60-03
sentinel_wait_for_replica_or_exit 172.20.1.101

docker-compose run ruby-client sidekiq-queues-scard

log_info failover to redis-01
docker-compose exec redis-60-02 redis-cli config set replica-priority 0
docker-compose exec redis-60-03 redis-cli config set replica-priority 0
sleep 3
docker-compose exec redis-sentinel-62-01 redis-cli -p 26379 sentinel failover mymaster
redis_wait_for_stepdown_or_exit redis-60-02
redis_wait_for_stepdown_or_exit redis-60-03
redis_wait_for_connected_or_exit redis-62-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-60-03
sentinel_wait_for_master_or_exit redis-sentinel-62-01 172.20.1.101
docker-compose exec redis-60-02 redis-cli config set replica-priority 100
docker-compose exec redis-60-03 redis-cli config set replica-priority 100
docker-compose exec redis-sentinel-62-01 redis-cli -p 26379 sentinel get-master-addr-by-name mymaster

docker-compose run ruby-client sidekiq-queues-scard

log_info failover away from redis-01
docker-compose exec redis-62-01 redis-cli config set replica-priority 0
sleep 3
docker-compose exec redis-sentinel-62-01 redis-cli -p 26379 sentinel failover mymaster
redis_wait_for_stepdown_or_exit redis-62-01
redis_wait_for_connected_or_exit redis-62-01
redis_wait_for_connected_or_exit redis-60-02
redis_wait_for_connected_or_exit redis-60-03
sentinel_wait_for_replica_or_exit 172.20.1.101
docker-compose exec redis-62-01 redis-cli config set replica-priority 100
docker-compose exec redis-sentinel-62-01 redis-cli -p 26379 sentinel get-master-addr-by-name mymaster

docker-compose run ruby-client sidekiq-queues-scard

log_info read-sequence
docker-compose stop ruby-client-write-sequence
docker-compose run ruby-client-read-sequence

# data integrity check
log_info dbsize
docker-compose exec redis-62-01 redis-cli dbsize
docker-compose exec redis-62-01 redis-cli save
docker-compose run save-dump-rdb "01"
docker-compose start redis-integrity-01
docker-compose start redis-integrity-02
redis_wait_for_pong_or_exit redis-integrity-01
redis_wait_for_pong_or_exit redis-integrity-02
docker-compose run ruby-client-integrity

# shutdown procedure
docker-compose down

# wait for docker-compose logs -f to complete
# wait

log_info 'done'
