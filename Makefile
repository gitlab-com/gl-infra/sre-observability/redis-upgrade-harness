.PHONY:
test: docker-build
	tests/sentinel-upgrade-downgrade.sh
	tests/redis-upgrade-downgrade.sh
	tests/redis-upgrade-single.sh

.PHONY:
docker-build: .docker-build

.docker-build:
	docker build -t igorwgitlab/redis-upgrade-harness-ruby-client ruby-client
	touch .docker-build

.PHONY:
docker-rebuild:
	docker build -t igorwgitlab/redis-upgrade-harness-ruby-client ruby-client

.PHONY:
lint:
	shellcheck tests/*.sh

.PHONY:
clean:
	docker-compose down --volumes
