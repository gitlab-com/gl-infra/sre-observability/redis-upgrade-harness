# redis-upgrade-harness

This is a test harness for testing redis upgrade paths.

It is intended to be self-contained.

## Usage

Run test suite:

```
make test
```

Run individual steps:

```
make docker-rebuild

docker-compose down --volumes
docker-compose up -d redis-60-01 redis-60-02 redis-60-03
docker-compose up -d redis-sentinel-60-01 redis-sentinel-60-02 redis-sentinel-60-03
docker-compose run ruby-client sidekiq-queues-scard

docker-compose exec redis-sentinel-60-01 redis-cli -p 26379 sentinel get-master-addr-by-name mymaster

docker-compose exec redis-60-01 redis-cli role

docker-compose logs

make clean
```

Get an rdb dump from gdk to import:

```
echo 'dbfilename dump.rdb' >> redis/redis.conf
echo 'rdbcompression yes' >> redis/redis.conf
echo 'rdbchecksum yes' >> redis/redis.conf
gdk restart redis
redis-cli -s redis/redis.socket save
ls -lah dump.rdb

cp dump.rdb redis-upgrade-harness/data
```

## Rebuilding the docker image

The `ruby-client` docker image is built via:

```
make docker-build
```

However, once it has been built, we don't automatically detect changes. To
rebuild it after updating anything in `ruby-client`, you need to use:

```
make docker-rebuild
```

## Ownership

* @mwasilewski
* @igorwwwwwwwwwwwwwwwwwwww
